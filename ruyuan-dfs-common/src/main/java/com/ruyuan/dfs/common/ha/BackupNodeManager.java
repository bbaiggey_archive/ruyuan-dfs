package com.ruyuan.dfs.common.ha;

import com.ruyuan.dfs.common.network.NetClient;
import com.ruyuan.dfs.common.utils.DefaultScheduler;
import com.ruyuan.dfs.model.backup.BackupNodeInfo;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;

/**
 * 负责和BackupNode进行连接的管理器
 *
 * @author Sun Dasheng
 */
@Slf4j
public class BackupNodeManager {

    private DefaultScheduler defaultScheduler;
    private volatile NetClient netClient;
    private ReportNameNodeStatusHandler reportNameNodeStatusHandler;

    public BackupNodeManager(DefaultScheduler defaultScheduler) {
        this.defaultScheduler = defaultScheduler;
    }

    /**
     * 和BackupNode建立连接
     * 当BackupNode进行主备切换的时候 会先将自己原来的BackupServer停掉-->与BackupServer连接的所有节点都将会被感知，进行角色切换 连接新的NameNode节点
     * 之后以全新的NameNode角色启动
     *
     * @param backupNodeInfo backupNode结果
     */
    public void maybeEstablishConnect(BackupNodeInfo backupNodeInfo, BackupUpGradeListener backupUpGradeListener) {
        if (netClient == null) {
            synchronized (this) {
                if (netClient == null) {
                    netClient = new NetClient("DataNode-BackupNod-" + backupNodeInfo.getHostname(), defaultScheduler, 3);
                    reportNameNodeStatusHandler = new ReportNameNodeStatusHandler();
                    netClient.addHandlers(Collections.singletonList(reportNameNodeStatusHandler));
                    netClient.connect(backupNodeInfo.getHostname(), backupNodeInfo.getPort());
                    log.info("收到NameNode返回的BackupNode信息，建立链接：[hostname={}, port={}]",
                            backupNodeInfo.getHostname(), backupNodeInfo.getPort());
                    netClient.addNetClientFailListener(() -> {
                        reset();
                        if (backupUpGradeListener != null) {
                            backupUpGradeListener.onBackupUpGrade(backupNodeInfo.getHostname());
                        }
                    });
                }
            }
        }
    }

    private void reset() {
        this.netClient.shutdown();
        this.netClient = null;
        this.reportNameNodeStatusHandler = null;
    }

    /**
     * 标识NameNode节点宕机了
     */
    public void markNameNodeDown() {
        if (reportNameNodeStatusHandler != null) {
            reportNameNodeStatusHandler.markNameNodeDown();
        }
    }
}
